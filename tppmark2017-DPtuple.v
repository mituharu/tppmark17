(* TPPmark 2017 DP list/tuple version by Mitsuharu Yamamoto *)
From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section SetTnthDef.

Variable n : nat.
Variable T : Type.
Implicit Type t : n.-tuple T.

Lemma set_nth_tupleP x0 t (i : 'I_n) y : size (set_nth x0 t i y) == n.
Proof.  by rewrite size_set_nth size_tuple (maxn_idPr (valP i)).  Qed.
Canonical set_nth_tuple x0 t i y := Tuple (set_nth_tupleP x0 t i y).

Definition set_tnth t i y := [tuple of set_nth (tnth_default t i) t i y].

End SetTnthDef.

Section TupleExtra.

Variable n : nat.
Variable T : Type.
Implicit Type t : n.-tuple T.

Lemma tnth_lift0 x t (i : 'I_n) :
  tnth [tuple of x :: t] (lift ord0 i) = tnth t i.
Proof.  by rewrite !(tnth_nth (tnth_default t i)).  Qed.

Lemma map_cons_tuple rT x (f : T -> rT) t :
  [tuple of map f (x :: t)] = [tuple of f x :: map f t].
Proof.  exact: val_inj.  Qed.

Lemma tnth_set_tnth t i y : tnth (set_tnth t i y) =1 [eta tnth t with i |-> y].
Proof.
  by move=> j /=; rewrite /set_tnth !(tnth_nth (tnth_default t i)) nth_set_nth.
Qed.

CoInductive set_tnth_cons_spec x t i y : (n.+1).-tuple T -> Prop :=
| SetTnthCons0 of i = ord0 :
    set_tnth_cons_spec x t i y [tuple of y :: t]
| SetTnthConsLift0 j of i = lift ord0 j :
    set_tnth_cons_spec x t i y [tuple of x :: set_tnth t j y].

Lemma set_tnth_consP x t i y :
  set_tnth_cons_spec x t i y (set_tnth [tuple of x :: t] i y).
Proof.
  case: (unliftP ord0 i) => [i' |] ->.
  - rewrite [set_tnth _ _ _](_ : _ = [tuple of x :: set_tnth t i' y]).
    + by constructor 2.
    + apply: val_inj => /=; congr cons.
      move: (tnth_default _ _) (tnth_default _ _) => d2 d1.
      have Hsize d : size (set_nth d t i' y) = n
        by rewrite size_set_nth size_tuple (maxn_idPr (valP i')).
      apply: (@eq_from_nth _ d2) => [| j Hj]; first by rewrite !Hsize.
      rewrite (set_nth_default d1) // !nth_set_nth /= (set_nth_default d1) //.
      by move: Hj; rewrite size_tuple Hsize.
  - rewrite [set_tnth _ _ _](_ : _ = [tuple of y :: t]); last by apply: val_inj.
    by constructor.
Qed.

End TupleExtra.

(* List version; suitable for computation for concrete values *)

Section LongestCommonSubsequenceForList.

Fixpoint argmaxl (X : Type) f (x : X) ys :=
  if ys is y :: ys' then argmaxl f (if f x < f y then y else x) ys'
  else x.

Lemma argmaxl_sup (X : Type) f (x : X) ys m :
  (m <= f x) || has (fun y => m <= f y) ys -> m <= f (argmaxl f x ys).
Proof.
  elim: ys => [| y ys IHys] /= in x *; first by rewrite orbF.
  move=> H; case: ifP => Hxy.
  - by apply IHys; case/orP: H => // H; rewrite (ltnW (leq_ltn_trans H Hxy)).
  - apply IHys; rewrite orbCA in H; case/orP: H => // H.
    by rewrite (leq_trans H) // leqNgt Hxy.
Qed.

Lemma argmaxl_mem (X : eqType) f (x : X) ys :
  (argmaxl f x ys == x) || (argmaxl f x ys \in ys).
Proof.
  elim: ys => [| y ys IHys] in x *; first by rewrite eqxx.
  rewrite /=; case: ifP => H; first by rewrite inE (IHys y) orbT.
  case/predU1P: (IHys x) => [-> | H']; first by rewrite eqxx.
  by rewrite inE H' !orbT.
Qed.

Variable T : eqType.

Definition NIL {n} : iter n seq (seq T) :=
  if n is 0 then [::] else [::].

Fixpoint nhead_simpl n : iter n seq (seq T) -> seq T :=
  if n is 0 then id
  else fun s => if s is x :: s' then nhead_simpl x else [::].

Definition nhead := nosimpl nhead_simpl.

Arguments nhead {n} s.

Lemma nhead_NIL n : @nhead n NIL = [::].
Proof.  by case: n.  Qed.

Lemma nhead_cons n x s : @nhead n.+1 (x :: s) = nhead x.
Proof.  by [].  Qed.

Definition nheadE := (nhead_NIL, nhead_cons).

Lemma nhead_head n s : @nhead n.+1 s = nhead (head NIL s).
Proof.  by case: s => [| ? ?]; rewrite !nheadE.  Qed.

Definition LCSL0 x ys diag xneighbor neighbors : seq T :=
  if all (pred1 x) ys then x :: diag else argmaxl size xneighbor neighbors.

Fixpoint LCSLts_simpl ts x ys :
  iter (size ts) seq (seq T) ->       (* diag *)
  iter (size ts) seq (seq T) ->       (* xneighbor *)
  seq (iter (size ts) seq (seq T)) -> (* neighbors *)
  iter (size ts) seq (seq T) :=
  if ts is t :: ts' then
    fun diag xneighbor neighbors =>
      let fix LCSLt t diag xneighbor neighbors :=
          if t is y :: t' then
            let prev := LCSLt t' (behead diag) (behead xneighbor)
                              (map behead neighbors) in
            (@LCSLts_simpl ts' x (y :: ys) (head NIL (behead diag))
                           (head NIL xneighbor)
                           (head NIL prev :: map (head NIL) neighbors))
              :: prev
          else [::]
      in LCSLt t diag xneighbor neighbors
  else
    fun diag xneighbor neighbors =>
      LCSL0 x ys diag xneighbor neighbors.

Definition LCSLts := nosimpl LCSLts_simpl.

Arguments LCSLts : clear implicits.

Lemma LCSLts_nil x ys diag xneighbor neighbors :
  LCSLts [::] x ys diag xneighbor neighbors =
  LCSL0 x ys diag xneighbor neighbors.
Proof.  by [].  Qed.

Lemma LCSLts_cons_nil ts x ys diag xneighbor neighbors :
  LCSLts ([::] :: ts) x ys diag xneighbor neighbors = [::].
Proof.  by [].  Qed.

Lemma LCSLts_cons_cons y t ts x ys diag xneighbor neighbors :
  LCSLts ((y :: t) :: ts) x ys diag xneighbor neighbors =
  let prev := LCSLts (t :: ts) x ys (behead diag) (behead xneighbor)
                     (map behead neighbors) in
  (LCSLts ts x (y :: ys) (head NIL (behead diag)) (head NIL xneighbor)
          (head NIL prev :: map (head NIL) neighbors)) :: prev.
Proof.  by [].  Qed.

Definition LCSLtsE := (LCSLts_nil, LCSLts_cons_nil, LCSLts_cons_cons).

Fixpoint LCSLs s ts : iter (size ts) seq (seq T) :=
  if s is x :: s' then let prev := LCSLs s' ts in
                       LCSLts ts x [::] prev prev [::]
  else NIL.

Fixpoint LCSLs_debug s ts : seq (iter (size ts) seq (seq T)) :=
  if s is x :: s' then let prev := LCSLs_debug s' ts in
                       (LCSLts ts x [::] (head NIL prev) (head NIL prev) [::])
                         :: prev
  else [::].

Definition LCSL s ts := nhead (LCSLs s ts).

Definition LCSL_spec s ts (lcsl : seq T) :=
  [/\ subseq lcsl s, all (subseq lcsl) ts &
      forall u, subseq u s -> all (subseq u) ts -> size u <= size lcsl].

End LongestCommonSubsequenceForList.

Arguments NIL {T n}.
Arguments nhead {T n} s.

(* Tuple version; suitable for verification *)

Section LongestCommonSubsequenceForTuple.

Variable T : eqType.

Fixpoint LCSTts_simpl n x ys :
  n.-tuple (seq T) ->           (* ts *)
  iter n seq (seq T) ->         (* diag *)
  iter n seq (seq T) ->         (* xneighbor *)
  seq (iter n seq (seq T)) ->   (* neighbors *)
  iter n seq (seq T) :=
  if n is n'.+1 then
    fun ts diag xneighbor neighbors =>
      let (t, ts') := tupleP ts in
      let fix LCSTt t diag xneighbor neighbors :=
          if t is y :: t' then
            let prev := LCSTt t' (behead diag) (behead xneighbor)
                              (map behead neighbors) in
            LCSTts_simpl x (y :: ys) ts' (head NIL (behead diag))
                         (head NIL xneighbor)
                         (head NIL prev :: map (head NIL) neighbors) :: prev
          else [::]
      in LCSTt t diag xneighbor neighbors
  else
    fun ts diag xneighbor neighbors =>
      LCSL0 x ys diag xneighbor neighbors.

Definition LCSTts := nosimpl LCSTts_simpl.

Arguments LCSTts [n] x ys ts diag xneighbor neighbors.

Lemma LCSTts_nil x ys (ts : 0.-tuple (seq T)) diag xneighbor neighbors :
  LCSTts x ys ts diag xneighbor neighbors =
  LCSL0 x ys diag xneighbor neighbors.
Proof.  by [].  Qed.

Lemma LCSTts_cons_nil n x ys (ts : n.-tuple (seq T)) diag xneighbor neighbors :
  LCSTts x ys [tuple of [::] :: ts] diag xneighbor neighbors = [::].
Proof.
  rewrite /LCSTts /=.
  by case E: _ / tupleP => [? ?]; case/(congr1 val): E => <-.
Qed.

Lemma LCSTts_cons_cons n x ys y t (ts : n.-tuple (seq T))
      diag xneighbor neighbors :
  LCSTts x ys [tuple of (y :: t) :: ts] diag xneighbor neighbors =
  let prev := LCSTts x ys [tuple of t :: ts] (behead diag) (behead xneighbor)
                     (map behead neighbors) in
  LCSTts x (y :: ys) ts (head NIL (behead diag)) (head NIL xneighbor)
         (head NIL prev :: map (head NIL) neighbors) :: prev.
Proof.
  rewrite /LCSTts /=.
  case E: _ / tupleP => [? ?]; case/(congr1 val): E => <- /val_inj <-.
  by case E: _ / tupleP => [? ?]; case/(congr1 val): E => <- /val_inj <-.
Qed.

Definition LCSTtsE := (LCSTts_nil, LCSTts_cons_nil, LCSTts_cons_cons).

Fixpoint LCSTs n s (ts : n.-tuple (seq T)) : iter n seq (seq T) :=
  if s is x :: s' then let prev := LCSTs s' ts in
                       LCSTts x [::] ts prev prev [::]
  else NIL.

Definition LCST n s (ts : n.-tuple (seq T)) := nhead (LCSTs s ts).

Lemma lcst_nil_l n (ts : n.-tuple (seq T)) : LCST [::] ts = [::].
Proof.  by rewrite /LCST /LCSTs nheadE.  Qed.

Lemma lcst_nil_r n s (ts : n.-tuple (seq T)) : [::] \in ts -> LCST s ts = [::].
Proof.
  rewrite /LCST; case: s => [| x s] /=; first by rewrite nheadE.
  move: {2}[::] (Nil (iter n seq (seq T))) => ys diag.
  move: {1}(LCSTs _ _) {1}(LCSTs _ _) => xneighbor neighbors.
  elim: n => [| n IHn] in ys ts diag xneighbor neighbors *.
  - by rewrite tuple0.
  - case/tupleP: ts => -[| y t] ts; first by rewrite LCSTtsE.
    by rewrite inE => /predU1P [| Hts] //; rewrite LCSTtsE /= nheadE IHn.
Qed.

Fixpoint nbehead X n m : iter m seq X -> iter m seq X :=
  if m is m'.+1 then if n is n'.+1 then map (@nbehead X n' m') else behead
  else id.

Lemma nbehead_nil X n m : nbehead n ([::] : iter m.+1 seq X) = [::].
Proof.  by elim: n.  Qed.

Lemma nbehead_NIL n m : n < m -> nbehead n (NIL : iter m seq (seq T)) = NIL.
Proof.  by case: m => // m; elim: n.  Qed.

Lemma nbehead_behead X n m (s : iter m.+1 seq X) :
  n < m.+1 -> @nbehead _ n m.+1 (behead s) = behead (nbehead n s).
Proof.  by case: n s => [| n] [].  Qed.

Definition ibehead {X n} (i : 'I_n) (s : iter n seq X) := nbehead i s.

Lemma ibehead0 X n (s : iter n.+1 seq X) : ibehead ord0 s = behead s.
Proof.  by [].  Qed.

Lemma ibehead_lift0 X n i (s : iter n.+1 seq X) :
  ibehead (lift ord0 i) s = map (ibehead i) s.
Proof.  by [].  Qed.

Lemma ibehead_nil X n i : ibehead i ([::] : iter n.+1 seq X) = [::].
Proof.  by rewrite /ibehead nbehead_nil.  Qed.

Lemma ibehead_NIL n i : ibehead i (NIL : iter n seq (seq T)) = NIL.
Proof.  by rewrite /ibehead nbehead_NIL.  Qed.

Lemma ibehead_head n i (s : iter n.+1 seq (seq T)) :
  ibehead i (head NIL s) = head NIL (ibehead (lift ord0 i) s).
Proof.  by rewrite ibehead_lift0; case: s => //=; rewrite ibehead_NIL.  Qed.

Lemma ibehead_behead X n (i : 'I_n.+1) (s : iter n.+1 seq X) :
  ibehead i (behead s) = behead (ibehead i s).
Proof.  by rewrite /ibehead nbehead_behead.  Qed.

Definition set_tnth_behead n (ts : n.-tuple (seq T)) (i : 'I_n) :=
  set_tnth ts i (behead (tnth ts i)).

CoInductive set_tnth_behead_cons_spec n x (t : n.-tuple (seq T)) i :
  (n.+1).-tuple (seq T) -> Prop :=
| SetTnthBeheadCons0 of i = ord0 :
    set_tnth_behead_cons_spec x t i [tuple of behead x :: t]
| SetTnthBeheadConsLift0 j of i = lift ord0 j :
    set_tnth_behead_cons_spec x t i [tuple of x :: set_tnth_behead t j].

Lemma set_tnth_behead_consP n x (t : n.-tuple (seq T)) i :
  set_tnth_behead_cons_spec x t i (set_tnth_behead [tuple of x :: t] i).
Proof.
  rewrite /set_tnth_behead.
  case: set_tnth_consP => [| i'] ->; first by constructor.
  by rewrite tnth_lift0; constructor.
Qed.

Lemma ibehead_LCSTts n x ys (ts : n.-tuple (seq T)) diag xneighbor neighbors i:
  ibehead i (LCSTts x ys ts diag xneighbor neighbors) =
  LCSTts x ys (set_tnth_behead ts i)
         (ibehead i diag) (ibehead i xneighbor) (map (ibehead i) neighbors).
Proof.
  elim: n => [| n IHn] in i ys ts diag xneighbor neighbors *;
    first by move: (ltn_ord i).
  case/tupleP: ts => t ts.
  elim: t => [| y t IHt] in diag xneighbor neighbors i *; rewrite LCSTtsE /=;
    first by case: set_tnth_behead_consP => [| j] ->; rewrite LCSTtsE.
  case: set_tnth_behead_consP => [| j] -> //.
  rewrite ibehead_lift0 /= -ibehead_lift0 LCSTtsE /= IHn /=.
  rewrite !ibehead_head IHt !ibehead_behead.
  rewrite -!map_comp (eq_map (ibehead_behead _)) (eq_map (ibehead_head _)).
  by case: set_tnth_behead_consP => [| _ /lift_inj <-].
Qed.

Definition abehead {X n} (s : iter n seq X) :=
  foldr ibehead s (enum 'I_n).

Lemma abehead0 X (s : iter 0 seq X) : abehead s = s.
Proof.
  rewrite /abehead.
  by have ->: enum 'I_0 = [::] by rewrite enumT unlock.
Qed.

Lemma abehead_nil X n : abehead ([::] : iter n.+1 seq X) = [::].
Proof.
  rewrite /abehead.
  by elim: (enum 'I_n.+1) => [| j js IHjs] //=; rewrite IHjs ibehead_nil.
Qed.

Lemma abehead_NIL n : abehead (NIL : iter n seq (seq T)) = NIL.
Proof.
  rewrite /abehead.
  by elim: (enum 'I_n) => [| j js IHjs] //=; rewrite IHjs ibehead_NIL.
Qed.

Lemma abehead_cons X n x s : @abehead X n.+1 (x :: s) = map abehead s.
Proof.
  rewrite /abehead enum_ordS /= ibehead0.
  elim: (enum 'I_n) => [| j js IHjs] /=; first by rewrite map_id.
  by rewrite ibehead_lift0 behead_map IHjs -map_comp.
Qed.

Lemma abehead_head_behead X n d (s : iter n.+1 seq X) :
  abehead (head d (behead s)) = head (abehead d) (abehead s).
Proof.
  case: s => [| x s] /=; first by rewrite abehead_nil.
  by rewrite abehead_cons; case: s.
Qed.

Lemma abehead_behead X n (s : iter n.+1 seq X) :
  @abehead X n.+1 (behead s) = behead (abehead s).
Proof.
  case: n => [| n] in s *.
  - rewrite /abehead enum_ordS.
    have ->: enum 'I_0 = [::] by rewrite enumT unlock.
    by rewrite /= ibehead_behead.
  - case: s => [| x s] /=; first by rewrite abehead_nil.
    rewrite abehead_cons {x}.
    case: s => [| x s] /=; first by rewrite abehead_nil.
    by rewrite abehead_cons.
Qed.

Lemma abehead_LCSTts n x ys (ts : n.-tuple (seq T)) diag xneighbor neighbors:
  abehead (LCSTts x ys ts diag xneighbor neighbors) =
  LCSTts x ys [tuple of map behead ts]
         (abehead diag) (abehead xneighbor) (map abehead neighbors).
Proof.
  elim: n => [| n IHn] in ys ts diag xneighbor neighbors *.
  - by rewrite tuple0 /= !LCSTtsE !abehead0 (eq_map (@abehead0 _)) map_id.
  - case/tupleP: ts => t ts.
    elim: t => [| y t IHt] in diag xneighbor neighbors *.
    + by rewrite LCSTtsE map_cons_tuple /= LCSTtsE abehead_NIL.
    + rewrite LCSTtsE map_cons_tuple /= abehead_cons.
      case: t => [| y' t] in IHt diag neighbors *; first by rewrite !LCSTtsE.
      move/(_ (behead diag) (behead xneighbor) (map behead neighbors)) in IHt.
      rewrite LCSTtsE map_cons_tuple /= abehead_cons in IHt.
      rewrite !LCSTtsE /= IHt IHn /=.
      rewrite !abehead_head_behead abehead_NIL !abehead_behead.
      congr (LCSTts _ _ _ _ _ (_ :: _) :: @LCSTts _.+1 _ _ _ _ _ _).
      * case: t => [| y'' t] in IHt *; first by rewrite !LCSTtsE abehead_NIL.
        move/(congr1 (head NIL)): IHt; rewrite LCSTtsE /= => ->.
        by rewrite !abehead_behead -!map_comp (eq_map (@abehead_behead _ _)).
      * by rewrite -!map_comp (eq_map (abehead_head_behead _)) abehead_NIL.
      * by rewrite -!map_comp (eq_map (@abehead_behead _ _)).
Qed.

Lemma lcst_cons_aux n x ys (ts : n.-tuple (seq T)) diag xneighbor neighbors :
  [::] \notin ts ->
  nhead (LCSTts x ys ts diag xneighbor neighbors) =
  let hds := map ohead ts in
  if (all (pred1 x) ys) && (all (pred1 (Some x)) hds)
  then x :: nhead (abehead diag)
  else argmaxl size (nhead xneighbor)
               (catrev [tuple nhead
                              (LCSTts x ys (set_tnth_behead ts i)
                                      (ibehead i diag) (ibehead i xneighbor)
                                      (map (ibehead i) neighbors))
                       | i < n]
                       (map nhead neighbors)).
Proof.
  elim: n => [| n IHn] in ys ts diag xneighbor neighbors *.
  - move=> _; rewrite !tuple0 /= andbT !LCSTtsE /LCSL0 /= abehead0.
    have ->: enum 'I_0 = [::] by rewrite enumT unlock.
    by rewrite map_id.
  - case/tupleP: ts => -[| y t] ts //.
    rewrite inE orFb => Hts; rewrite LCSTtsE nheadE {}IHn //=.
    have ->: all (pred1 x) (y :: ys) && all (pred1 (Some x)) (map ohead ts) =
    all (pred1 x) ys && all (pred1 (Some x)) (map ohead ((y :: t) :: ts))
      by rewrite /= [in Some _ == Some _]/eq_op /= andbCA andbA.
    case: ifP => _; first by rewrite abehead_head_behead abehead_NIL nhead_head.
    rewrite -!nhead_head enum_ordS map_cons -!map_comp.
    congr (argmaxl _ _ (catrev _ (_ :: _))).
    + apply: eq_map => i /=.
      case: set_tnth_behead_consP => [| _ /lift_inj <-] //.
      rewrite LCSTtsE /= !ibehead_head ibehead_behead.
      rewrite ibehead_LCSTts !ibehead_behead.
      rewrite -!map_comp (eq_map (ibehead_behead _)) (eq_map (ibehead_head _)).
      by case: set_tnth_behead_consP => [| _ /lift_inj <-].
    + by case: set_tnth_behead_consP.
    + by rewrite (eq_map (@nhead_head _ _)).
Qed.

Lemma lcst_cons n x s (ts : n.-tuple (seq T)) :
  [::] \notin ts ->
  LCST (x :: s) ts =
  if all (pred1 (Some x)) (map ohead ts)
  then x :: LCST s [tuple of map behead ts]
  else argmaxl size (LCST s ts)
               (rev [tuple LCST (x :: s) (set_tnth_behead ts i) | i < n]).
Proof.
  move=> Hts; rewrite /LCST /= lcst_cons_aux //=.
  congr (if _ then _ :: nhead _ else argmaxl _ _ (rev _)).
  - elim: s => [| x' s IHs]; first by rewrite abehead_NIL.
    by rewrite abehead_LCSTts IHs.
  - apply: eq_map => i /=.
    suff ->: ibehead i (LCSTs s ts) = LCSTs s (set_tnth_behead ts i) by [].
    elim: s => [| x' s IHs]; first by rewrite ibehead_NIL.
    by rewrite /= ibehead_LCSTts IHs.
Qed.

Lemma lcst_correct_nil_l n (ts : n.-tuple (seq T)) : LCSL_spec [::] ts (LCST [::] ts).
Proof.
  rewrite lcst_nil_l; split; first by rewrite sub0seq.
  - by apply/allP => ?; rewrite sub0seq.
  - by move=> ?; rewrite subseq0 => /eqP ->.
Qed.

Lemma lcst_correct_nil_r n s (ts : n.-tuple (seq T)) : [::] \in ts -> LCSL_spec s ts (LCST s ts).
Proof.
  move=> Hts; rewrite lcst_nil_r //; split; first by rewrite sub0seq.
  - by apply/allP => ?; rewrite sub0seq.
  - by move=> ? _ /allP /(_ _ Hts); rewrite subseq0 => /eqP ->.
Qed.

Lemma lcst_correct n s (ts : n.-tuple (seq T)) : LCSL_spec s ts (LCST s ts).
Proof.
  pose mes (s : seq T) (ts : seq (seq T)) := size s + (\sum_(t <- ts) size t).
  elim: {s ts}(mes s ts) {-2}s {-2}ts (leqnn (mes s ts)) => [| m IHm] s ts.
  - rewrite leqn0 addn_eq0 size_eq0 => /andP [/eqP -> _].
    exact: lcst_correct_nil_l.
  - case: s => [| x s] Hmes; first by apply: lcst_correct_nil_l.
    case: (boolP ([::] \in ts)) => [| Hts]; first by apply: lcst_correct_nil_r.
    rewrite lcst_cons //. case: ifP => Hx.
    + have /IHm [IH1 IH2 IH3] : mes s (map behead ts) <= m.
      { move: Hmes; rewrite /mes /= addSn ltnS; apply: leq_trans.
        rewrite leq_add2l big_map leq_sum // => t.
        by rewrite size_behead leq_pred. }
      split; first by rewrite /= eqxx.
      * apply/allP => t Ht; move/allP/(_ (ohead t)): Hx.
        rewrite map_f //= => /(_ isT) H; case: t H => // _ t /eqP [] -> in Ht *.
        by rewrite /= eqxx (allP IH2) // (map_f behead Ht).
      * { case=> // z u; rewrite [subseq _ _] /=.
          case: ifP => [/eqP |] Hzx Hus Hzuts.
          - apply: IH3 => //; apply/allP => _ /mapP [t Ht ->].
            move/allP/(_ (ohead t)): Hx.
            rewrite map_f //= => /(_ isT) H; case: t H Ht => // _ t /eqP [] ->.
            by move/(allP Hzuts); rewrite Hzx /= eqxx.
          - apply: leqW; apply: IH3 => //; apply/allP => _ /mapP [t Ht ->].
            move/allP/(_ (ohead t)): Hx.
            rewrite map_f //= => /(_ isT) H; case: t H Ht => // y t /eqP [] ->.
            by move/(allP Hzuts); rewrite /= Hzx. }
    + have /IHm [IHs1 IHs2 IHs3] : mes s ts <= m
        by move: Hmes; rewrite /mes /= addSn ltnS.
      have Htsi (i : 'I_n) : mes (x :: s) (set_tnth_behead ts i) <= m.
      { move: Hmes; rewrite /mes [size _]/= addSn ltnS; apply: leq_trans.
        rewrite addSn -addnS leq_add2l; set ts' := set_tnth_behead _ _.
        have Hts' i0 : i0 != i :> nat -> nth (tnth_default ts i) ts' i0 =
                                         nth (tnth_default ts i) ts i0
          by move=> Hi0; rewrite nth_set_nth /= (negPf Hi0).
        rewrite !(big_nth (tnth_default ts i)).
        rewrite (@big_cat_nat _ _ _ i.+1) ?size_tuple // [ts']lock /=.
        rewrite [X in _ < X](@big_cat_nat _ _ _ i.+1) ?size_tuple //=.
        rewrite !big_nat_recr //= -addSn -addnS !big_nat !leq_add // -lock.
        - apply: eq_leq; apply: eq_bigr => i0 Hi0; rewrite Hts' //.
          by apply/negP => /eqP Hi0i; rewrite Hi0i ltnn andbF in Hi0.
        - rewrite nth_set_nth /= eqxx size_behead prednK //.
          case E: (tnth ts i) => //; case/negP: Hts; rewrite -E; apply/tnthP.
          by exists i.
        - apply: eq_leq; apply: eq_bigr => i0 Hi0; rewrite Hts' //.
          by apply/negP => /eqP Hi0i; rewrite Hi0i ltnn in Hi0. }
      set lts' := rev _; split.
      * { case/predU1P: (argmaxl_mem size (LCST s ts) lts') => [-> |].
          - by rewrite (subseq_trans IHs1) ?subseq_cons.
          - rewrite /lts' [rev _]/= -!map_rev.
            by case/mapP => i Hi ->; case/IHm: (Htsi i). }
      * case/predU1P: (argmaxl_mem size (LCST s ts) lts') => [-> |] //.
        rewrite /lts' [rev _]/= -!map_rev; case/mapP => i Hi ->.
        set ts' := set_tnth_behead _ _; set lt' := LCST _ _.
        apply/allP => _ /tnthP [j ->].
        case/IHm: (Htsi i) => _ /allP /(_ (tnth ts' j)).
        rewrite mem_tnth /ts' /set_tnth_behead tnth_set_tnth => /(_ isT) H _.
        rewrite /= -/(set_tnth_behead ts i) in H.
        case: ifP H => // /eqP ->; rewrite /lt' /ts'.
        by case: (tnth ts i) => // y t /subseq_trans -> //; rewrite subseq_cons.
      * { case=> // z u Hzuxs Hzuts; apply: argmaxl_sup; rewrite /= in Hzuxs.
          move: Hzuxs; case: ifP => [/eqP {z}-> | Hzx] in Hzuts *.
          - move=> Hus'; case/allPn: Hx => _ /mapP [t Ht ->].
            case: t Ht => [/(negP Hts) | y t Hyt] // => Hyx.
            rewrite /= /eq_op /= in Hyx; case/tnthP: (Hyt) => i Htnthi.
            apply/orP; right; apply/hasP.
            exists (tnth [tuple of lts'] (rev_ord i)).
            + by rewrite mem_tnth.
            + rewrite /lts' (tnth_nth [::]) (nth_rev [::]) size_tuple //.
              rewrite -[n - _]/(rev_ord _ : nat) rev_ordK nth_mktuple.
              case/IHm: (Htsi i) => _ _ -> //; first by rewrite /= eqxx.
              apply/allP => _ /tnthP [j] ->.
              rewrite /set_tnth_behead tnth_set_tnth /=.
              case: ifP => [_ | Hij].
              * move/allP/(_ (tnth ts i)): Hzuts.
                by rewrite -Htnthi Hyt /= eq_sym (negPf Hyx) => ->.
              * by move/allP/(_ (tnth ts j)): Hzuts => -> //; rewrite mem_tnth.
          - by move/IHs3 => ->. }
Qed.

Lemma LCSTts_LCSLts x ys ts diag xneighbor neighbors :
  LCSTts x ys (in_tuple ts) diag xneighbor neighbors =
  LCSLts x ys diag xneighbor neighbors.
Proof.
  elim: ts => [| t ts IHts] // in ys diag xneighbor neighbors *.
  have ->: in_tuple (t :: ts) = [tuple of t :: in_tuple ts] by apply: val_inj.
  elim: t => [| y t IHt] in diag xneighbor neighbors *.
  - by rewrite LCSTtsE LCSLtsE.
  - by rewrite LCSTtsE LCSLtsE /= IHts IHt.
Qed.

Lemma LCST_LCSL s ts : LCST s (in_tuple ts) = LCSL s ts.
Proof.
  by congr nhead; elim: s => [| x s IHs] //=; rewrite IHs LCSTts_LCSLts.
Qed.

Lemma lcsl_correct (s : seq T) ts : LCSL_spec s ts (LCSL s ts).
Proof.  by rewrite -LCST_LCSL; apply: lcst_correct.  Qed.

End LongestCommonSubsequenceForTuple.

Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [:: 2; 1; 3; 5] ].
     (* = [:: [:: [:: 2; 3; 5]; [:: 1; 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*       [:: [:: 2; 3; 5]; [:: 3; 5];    [:: 3; 5]; [:: 5]]; *)
     (*       [:: [:: 3; 5];    [:: 3; 5];    [:: 3; 5]; [:: 5]]; *)
     (*       [:: [:: 5];       [:: 5];       [:: 5];    [:: 5]]; *)
     (*       [:: [:: 5];       [:: 5];       [:: 5];    [:: 5]]] *)
     (* : seq (iter (size [:: [:: 2; 1; 3; 5]]) seq (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 3; 4; 5] [:: [:: 2; 1; 3; 5] ].
     (* = [:: [:: [:: 1; 3; 5]; [:: 1; 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*       [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*       [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*       [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]] *)
     (* : seq (iter (size [:: [:: 2; 1; 3; 5]]) seq (seq nat_eqType)) *)
Eval compute in LCSLs_debug [::] [:: [:: 2; 1; 3; 5] ].
     (* = [::] *)
     (* : seq (iter (size [:: [:: 2; 1; 3; 5]]) seq (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5] ].
     (* = [:: [:: [:: [:: 1; 3; 5]; [:: 1; 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]]; *)
     (*       [:: [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]]; *)
     (*       [:: [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]]; *)
     (*       [:: [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]]; *)
     (*       [:: [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]]] *)
     (* : seq *)
     (*     (iter (size [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]]) seq *)
     (*        (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [:: 1; 3; 4]; [:: 2; 1; 3; 5] ].
     (* = [:: [:: [:: [:: 1; 3]; [:: 1; 3]; [:: 3]; [::]]; *)
     (*           [:: [:: 3]; [:: 3]; [:: 3]; [::]]; [:: *)
     (*           [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [:: 3]; [:: 3]; [:: 3]; [::]]; *)
     (*           [:: [:: 3]; [:: 3]; [:: 3]; [::]]; [:: *)
     (*           [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [:: 3]; [:: 3]; [:: 3]; [::]]; *)
     (*           [:: [:: 3]; [:: 3]; [:: 3]; [::]]; [:: *)
     (*           [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]]] *)
     (* : seq *)
     (*     (iter (size [:: [:: 1; 3; 4]; [:: 2; 1; 3; 5]]) seq (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]; [:: 2; 1; 3]].
     (* = [:: [:: [:: [:: [:: 1; 3]; [:: 1; 3]; [:: 3]]; *)
     (*               [:: [:: 1; 3]; [:: 1; 3]; [:: 3]]; *)
     (*               [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [::]; [::]; [::]]]; *)
     (*           [:: [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [:: 3]; [:: 3]; [:: 3]]; [:: [:: 3]; [:: 3]; [:: 3]]; *)
     (*               [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]]; *)
     (*       [:: [:: [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [:: 3]; [:: 3]; [:: 3]]; [:: [:: 3]; [:: 3]; [:: 3]]; *)
     (*               [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [:: 3]; [:: 3]; [:: 3]]; [:: [:: 3]; [:: 3]; [:: 3]]; *)
     (*               [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]]; *)
     (*       [:: [:: [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [:: 3]; [:: 3]; [:: 3]]; [:: [:: 3]; [:: 3]; [:: 3]]; *)
     (*               [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [:: 3]; [:: 3]; [:: 3]]; [:: [:: 3]; [:: 3]; [:: 3]]; *)
     (*               [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]]; *)
     (*       [:: [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]]; *)
     (*       [:: [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]]] *)
     (* : seq *)
     (*     (iter (size [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]; [:: 2; 1; 3]]) seq *)
     (*        (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]; [::]].
     (* = [:: [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]]] *)
     (* : seq *)
     (*     (iter (size [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]; [::]]) seq *)
     (*        (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [::]; [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]].
     (* = [:: [::]; [::]; [::]; [::]; [::]] *)
     (* : seq *)
     (*     (iter (size [:: [::]; [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]]) seq *)
     (*        (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [::].
     (* = [:: [:: 1; 2; 3; 4; 5]; [:: 2; 3; 4; 5]; [:: *)
     (*       3; 4; 5]; [:: 4; 5]; [:: 5]] *)
     (* : seq (iter (size [::]) seq (seq nat_eqType)) *)
