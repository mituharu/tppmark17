# [TPPmark2017](https://aigarashi.github.io/TPP2017/) 解答

* 自然な再帰的定義 tppmark2017-Function.v
	* 列n(≥1)個に拡張
* 再帰による部分列全生成 tppmark2017-finType.v
	* 列n(≥1)個に拡張
* maskによる部分列網羅 tppmark2017-mask.v
	* 列n(≥1)個に拡張
* 動的計画法(DP) tppmark2017-DP.v
	* 列n(≥1)個に拡張 ~~ただし拡張版の証明は未完成~~ 完成しました!!
* 依存型版動的計画法 tppmark2017-DTDP.v
