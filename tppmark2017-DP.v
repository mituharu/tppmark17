(* TPPmark 2017 DP version by Mitsuharu Yamamoto *)
From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section DynamicProgramming.

Variables X Y Z : Type.

Variable z00 : Z.
Variable zc0 : X -> seq X -> Z -> Z.
Variable z0c : Y -> seq Y -> Z -> Z.
Variable zcc : X -> seq X -> Y -> seq Y -> Z -> Z -> Z -> Z.

Fixpoint dp0t t :=
  if t is y :: t' then let: (z, zs) := dp0t t' in (z0c y t' z, z :: zs)
  else (z00, [::]).

Fixpoint dpt x s t zs :=
  match t, zs with
  | y :: t', (z, z' :: zs') => let: (zy, zt') := dpt x s t' (z', zs') in
                               (zcc x s y t' z' z zy, zy :: zt')
  | _, (z, _) => (zc0 x s z, [::])
  end.

Fixpoint dps s t := if s is x :: s' then dpt x s' t (dps s' t) else dp0t t.

Definition dp s t := (dps s t).1.

Lemma dp00 : dp [::] [::] = z00.
Proof.  by [].  Qed.

Lemma dp0c y t : dp [::] (y :: t) = z0c y t (dp [::] t).
Proof.  by rewrite /dp /=; case: dp0t.  Qed.

Lemma dpc0 x s : dp (x :: s) [::] = zc0 x s (dp s [::]).
Proof.
  by rewrite /dp; elim: s => [| x' s ->] //= in x *; case: dps => ? [| ? ?].
Qed.

Lemma dpscc x y s t :
  dps (x :: s) (y :: t) = (zcc x s y t (dp s t) (dp s (y :: t)) (dp (x :: s) t),
                           dp (x :: s) t :: (dps (x :: s) t).2).
Proof.
  rewrite /dp; elim: s => /= [| x' s ->] in x *.
  - by case: dp0t => ? [| ? ?]; case: dpt.
  - by rewrite -surjective_pairing; case: dpt.
Qed.

Lemma dpcc x y s t :
  dp (x :: s) (y :: t) = zcc x s y t (dp s t) (dp s (y :: t)) (dp (x :: s) t).
Proof.  by rewrite /dp dpscc.  Qed.

End DynamicProgramming.

Section LongestCommonSubsequence.

Definition argmax (X : Type) f (x y : X) := if f x < f y then y else x.

Lemma argmax_maxn (X : Type) f (x y : X) : f (argmax f x y) = maxn (f x) (f y).
Proof.  by rewrite /argmax fun_if.  Qed.

Variable T : eqType.

Definition LCS : seq T -> seq T -> seq T :=
  dp [::] (fun _ _ _ => [::]) (fun _ _ _ => [::])
     (fun x _ y _ st syt xst =>
        if x == y then x :: st else argmax size syt xst).

Lemma lcs_nil_l s : LCS [::] s = [::].
Proof.  by rewrite /LCS; case: s => [| ? ?]; rewrite ?dp00 ?dp0c.  Qed.

Lemma lcs_nil_r s : LCS s [::] = [::].
Proof.  by rewrite /LCS; case: s => [| ? ?]; rewrite ?dp00 ?dpc0.  Qed.

Lemma lcs_cons x s y t :
  LCS (x :: s) (y :: t) = if x == y then x :: LCS s t
                          else argmax size (LCS s (y :: t)) (LCS (x :: s) t).
Proof.  by rewrite /LCS dpcc.  Qed.

(* Alternatively,
Fixpoint LCS2 x us t : seq (seq T) :=
  if t is y :: t' then let p := LCS2 x (behead us) t' in
                         (if x == y then x :: head [::] (behead us)
                          else argmax size (head [::] us) (head [::] p)) :: p
  else [::].

Fixpoint LCS1 s t : seq (seq T) :=
  if s is x :: s' then LCS2 x (LCS1 s' t) t else [::].

Definition LCS s t := head [::] (LCS1 s t).

Lemma lcs_nil_l s : LCS [::] s = [::].
Proof.  by [].  Qed.

Lemma lcs_nil_r s : LCS s [::] = [::].
Proof.  by rewrite /LCS; case: s => [| x s] //=; case: LCS1.  Qed.

Lemma lcs_cons_aux x s y t :
  LCS1 (x :: s) (y :: t) = (if x == y then x :: (LCS s t)
                            else argmax size (LCS s (y :: t)) (LCS (x :: s) t))
                             :: LCS1 (x :: s) t.
Proof.  by rewrite /LCS; elim: s => [| x' s /= /(_ x') [-> ->]] // in x *.  Qed.

Lemma lcs_cons x s y t :
  LCS (x :: s) (y :: t) = if x == y then x :: LCS s t
                          else argmax size (LCS s (y :: t)) (LCS (x :: s) t).
Proof.  by rewrite /LCS lcs_cons_aux.  Qed.
*)

Lemma lcs_subseq s t : subseq (LCS s t) s /\ subseq (LCS s t) t.
Proof.
  elim: s => [| x s IHs] in t *; first by rewrite lcs_nil_l !sub0seq.
  elim: t => [| y t [IHts IHtt]]; first by rewrite lcs_nil_r sub0seq.
  rewrite lcs_cons; case: ifP => [/eqP -> | Hxy]; first by rewrite /= eqxx.
  case/IHs: (y :: t) => IHss IHst; rewrite /argmax; split; case: ifP => _ //.
  - by rewrite (subseq_trans IHss) // subseq_cons.
  - by rewrite (subseq_trans IHtt) // subseq_cons.
Qed.

Lemma lcs_longest s t u : subseq u s -> subseq u t -> size u <= size (LCS s t).
Proof.
  elim: s => [| x s IHs] in u t *; first by move=> /eqP ->.
  elim: t => [| y t IHt] in u *; first by move=> _ /eqP ->.
  case: u => [| z u] //; rewrite lcs_cons; case: ifP => [/eqP <- {y} | Hxy] /=.
  - by move=> Hs Ht; move: (IHs _ _ Hs Ht); case: ifP => _ // /leqW.
  - rewrite argmax_maxn leq_max; case: ifP => [/eqP -> | _] Hs.
    + by rewrite Hxy => Ht; rewrite (IHt (x :: u)) ?orbT //= eqxx.
    + by move=> Ht; rewrite (IHs (z :: u)).
Qed.

End LongestCommonSubsequence.

Section LongestCommonSubsequenceForList.

Fixpoint argmaxl (X : Type) f (x : X) ys :=
  if ys is y :: ys' then argmaxl f (if f x < f y then y else x) ys'
  else x.

Lemma argmaxl_sup (X : Type) f (x : X) ys m :
  (m <= f x) || has (fun y => m <= f y) ys -> m <= f (argmaxl f x ys).
Proof.
  elim: ys => [| y ys IHys] /= in x *; first by rewrite orbF.
  move=> H; case: ifP => Hxy.
  - by apply IHys; case/orP: H => // H; rewrite (ltnW (leq_ltn_trans H Hxy)).
  - apply IHys; rewrite orbCA in H; case/orP: H => // H.
    by rewrite (leq_trans H) // leqNgt Hxy.
Qed.

Lemma argmaxl_mem (X : eqType) f (x : X) ys :
  (argmaxl f x ys == x) || (argmaxl f x ys \in ys).
Proof.
  elim: ys => [| y ys IHys] in x *; first by rewrite eqxx.
  rewrite /=; case: ifP => H; first by rewrite inE (IHys y) orbT.
  case/predU1P: (IHys x) => [-> | H']; first by rewrite eqxx.
  by rewrite inE H' !orbT.
Qed.

Variable T : eqType.

Definition NIL {n} : iter n seq (seq T) :=
  if n is 0 then [::] else [::].

Fixpoint nhead_simpl n : iter n seq (seq T) -> seq T :=
  if n is 0 then id
  else fun s => if s is x :: s' then nhead_simpl x else [::].

Definition nhead := nosimpl nhead_simpl.

Arguments nhead {n} s.

Lemma nhead_NIL n : @nhead n NIL = [::].
Proof.  by case: n.  Qed.

Lemma nhead_cons n x s : @nhead n.+1 (x :: s) = nhead x.
Proof.  by [].  Qed.

Definition nheadE := (nhead_NIL, nhead_cons).

Lemma nhead_head n s : @nhead n.+1 s = nhead (head NIL s).
Proof.  by case: s => [| ? ?]; rewrite !nheadE.  Qed.

Definition LCSL0 x ys diag xneighbor neighbors : seq T :=
  if all (pred1 x) ys then x :: diag else argmaxl size xneighbor neighbors.

Fixpoint LCSLts_simpl ts x ys :
  iter (size ts) seq (seq T) ->       (* diag *)
  iter (size ts) seq (seq T) ->       (* xneighbor *)
  seq (iter (size ts) seq (seq T)) -> (* neighbors *)
  iter (size ts) seq (seq T) :=
  if ts is t :: ts' then
    fun diag xneighbor neighbors =>
      let fix LCSLt t diag xneighbor neighbors :=
          if t is y :: t' then
            let prev := LCSLt t' (behead diag) (behead xneighbor)
                              (map behead neighbors) in
            (@LCSLts_simpl ts' x (y :: ys) (head NIL (behead diag))
                           (head NIL xneighbor)
                           (head NIL prev :: map (head NIL) neighbors))
              :: prev
          else [::]
      in LCSLt t diag xneighbor neighbors
  else
    fun diag xneighbor neighbors =>
      LCSL0 x ys diag xneighbor neighbors.

Definition LCSLts := nosimpl LCSLts_simpl.

Arguments LCSLts : clear implicits.

Lemma LCSLts_nil x ys diag xneighbor neighbors :
  LCSLts [::] x ys diag xneighbor neighbors =
  LCSL0 x ys diag xneighbor neighbors.
Proof.  by [].  Qed.

Lemma LCSLts_cons_nil ts x ys diag xneighbor neighbors :
  LCSLts ([::] :: ts) x ys diag xneighbor neighbors = [::].
Proof.  by [].  Qed.

Lemma LCSLts_cons_cons y t ts x ys diag xneighbor neighbors :
  LCSLts ((y :: t) :: ts) x ys diag xneighbor neighbors =
  let prev := LCSLts (t :: ts) x ys (behead diag) (behead xneighbor)
                     (map behead neighbors) in
  (LCSLts ts x (y :: ys) (head NIL (behead diag)) (head NIL xneighbor)
          (head NIL prev :: map (head NIL) neighbors)) :: prev.
Proof.  by [].  Qed.

Definition LCSLtsE := (LCSLts_nil, LCSLts_cons_nil, LCSLts_cons_cons).

Fixpoint LCSLs s ts : iter (size ts) seq (seq T) :=
  if s is x :: s' then let prev := LCSLs s' ts in
                       LCSLts ts x [::] prev prev [::]
  else NIL.

Fixpoint LCSLs_debug s ts : seq (iter (size ts) seq (seq T)) :=
  if s is x :: s' then let prev := LCSLs_debug s' ts in
                       (LCSLts ts x [::] (head NIL prev) (head NIL prev) [::])
                         :: prev
  else [::].

Definition LCSL s ts := nhead (LCSLs s ts).

Lemma lcsl_nil_l ts : LCSL [::] ts = [::].
Proof.  by rewrite /LCSL /LCSLs nheadE.  Qed.

Lemma lcsl_nil_r s ts : [::] \in ts -> LCSL s ts = [::].
Proof.
  rewrite /LCSL; case: s => [| x s] /=; first by rewrite nheadE.
  move: {2}[::] (Nil (iter (size ts) seq (seq T))) => ys diag.
  move: {1}(LCSLs _ _) {1}(LCSLs _ _) => xneighbor neighbors.
  elim: ts => [|[| y t] ts IHts] //= in ys diag xneighbor neighbors *.
  by rewrite inE => /predU1P [| Hts] //; rewrite LCSLtsE /= nheadE IHts.
Qed.

Section NestedSequenceCast.

Variable X : Type.

Definition nscast m n (eq_mn : m = n) s := ecast n (iter n seq X) eq_mn s.

Lemma nscast_id n (eq_nn : n = n) s : nscast eq_nn s = s.
Proof.  by rewrite (eq_axiomK eq_nn).  Qed.

Lemma nscastK m n (eq_mn : m = n) :
  cancel (nscast eq_mn) (nscast (esym eq_mn)).
Proof.  by case: n / eq_mn.  Qed.

Lemma nscastKV m n (eq_mn : m = n) :
  cancel (nscast (esym eq_mn)) (nscast eq_mn).
Proof.  by case: n / eq_mn.  Qed.

Lemma nscast_trans m n p (eq_mn : m = n) (eq_np : n = p) s:
  nscast (etrans eq_mn eq_np) s = nscast eq_np (nscast eq_mn s).
Proof.  by case: n / eq_mn eq_np; case: p /.  Qed.

Lemma nscast_nil m n (eq_m1n1 : m.+1 = n.+1) :
  nscast eq_m1n1 ([::] : iter m.+1 seq X) = [::].
Proof.
  by case: eq_m1n1 (eq_m1n1); case: _ / => eq_m1n1; rewrite !nscast_id.
Qed.

Lemma nscast_cons m n (eq_mn : m = n) (eq_m1n1 : m.+1 = n.+1)
      x (s : iter m.+1 seq X) :
  nscast eq_m1n1 (x :: s) = (nscast eq_mn x) :: (nscast eq_m1n1 s).
Proof.  by case: _ / eq_mn in eq_m1n1 *; rewrite !nscast_id.  Qed.

Lemma nscast_behead m n (eq_m1n1 : m.+1 = n.+1) s :
  nscast eq_m1n1 (@behead (iter m seq X) s) =
  @behead (iter n seq X) (nscast eq_m1n1 s).
Proof.
  by case: eq_m1n1 (eq_m1n1); case: _ / => eq_m1n1; rewrite !nscast_id.
Qed.

End NestedSequenceCast.

Lemma nscast_NIL m n (eq_mn : m = n) : nscast eq_mn NIL = NIL.
Proof.  by case: _ / eq_mn.  Qed.

Lemma nscast_head m n (eq_mn : m = n) (eq_m1n1 : m.+1 = n.+1)
      (s : iter m.+1 seq (seq T)) :
  nscast eq_mn (head NIL s) = head NIL (nscast eq_m1n1 s).
Proof.  by case: _ / eq_mn in eq_m1n1 *; rewrite !nscast_id.  Qed.

Fixpoint nbehead X n m : iter m seq X -> iter m seq X :=
  if m is m'.+1 then if n is n'.+1 then map (@nbehead X n' m') else behead
  else id.

Lemma nbehead_nil X n m : nbehead n ([::] : iter m.+1 seq X) = [::].
Proof.  by elim: n.  Qed.

Lemma nbehead_NIL n m : n < m -> nbehead n (NIL : iter m seq (seq T)) = NIL.
Proof.  by case: m => // m; elim: n.  Qed.

Lemma nbehead_behead X n m (s : iter m.+1 seq X) :
  n < m.+1 -> @nbehead _ n m.+1 (behead s) = behead (nbehead n s).
Proof.  by case: n s => [| n] [].  Qed.

Definition ibehead {X n} (i : 'I_n) (s : iter n seq X) := nbehead i s.

Lemma ibehead0 X n (s : iter n.+1 seq X) : ibehead ord0 s = behead s.
Proof.  by [].  Qed.

Lemma ibehead_lift0 X n i (s : iter n.+1 seq X) :
  ibehead (lift ord0 i) s = map (ibehead i) s.
Proof.  by [].  Qed.

Lemma ibehead_nil X n i : ibehead i ([::] : iter n.+1 seq X) = [::].
Proof.  by rewrite /ibehead nbehead_nil.  Qed.

Lemma ibehead_NIL n i : ibehead i (NIL : iter n seq (seq T)) = NIL.
Proof.  by rewrite /ibehead nbehead_NIL.  Qed.

Lemma ibehead_head n i (s : iter n.+1 seq (seq T)) :
  ibehead i (head NIL s) = head NIL (ibehead (lift ord0 i) s).
Proof.  by rewrite ibehead_lift0; case: s => //=; rewrite ibehead_NIL.  Qed.

Lemma ibehead_behead X n (i : 'I_n.+1) (s : iter n.+1 seq X) :
  ibehead i (behead s) = behead (ibehead i s).
Proof.  by rewrite /ibehead nbehead_behead.  Qed.

Lemma lcsl_ts_size (ts : seq (seq T)) (i : 'I_(size ts)) :
  size (set_nth [::] ts i (behead (nth [::] ts i))) = size ts.
Proof.   by rewrite size_set_nth (maxn_idPr (valP i)).  Qed.

Lemma ibehead_LCSLts ts x ys diag xneighbor neighbors i:
  ibehead i (LCSLts ts x ys diag xneighbor neighbors) =
  nscast
    (lcsl_ts_size i)
    (LCSLts (set_nth [::] ts i (behead (nth [::] ts i))) x ys
            (nscast (esym (lcsl_ts_size i)) (ibehead i diag))
            (nscast (esym (lcsl_ts_size i)) (ibehead i xneighbor))
            (map (nscast (esym (lcsl_ts_size i)) \o ibehead i) neighbors)).
Proof.
  elim: ts => [| t ts IHts] in ys diag xneighbor neighbors i *;
    first by move: (ltn_ord i).
  elim: t => [| y t IHt] in diag xneighbor neighbors i *; rewrite LCSLtsE /=;
    first by case : i => [[|n] Hi] /=; rewrite LCSLtsE ibehead_NIL nscast_NIL.
  case: (unliftP ord0 i) => [j |] ->.
  - rewrite ibehead_lift0 /= LCSLtsE IHts /= (nscast_cons (lcsl_ts_size j)).
    rewrite !ibehead_head IHt.
    set p1 := lcsl_ts_size (lift _ _); set p2 := lcsl_ts_size (lift _ _).
    rewrite !(nscast_head _ (esym p1)) !ibehead_behead !nscast_behead.
    have Hcast (s : iter (size ts).+1 seq (seq T)) :
      nscast (esym p1) (ibehead (lift ord0 j) s)
      = nscast (esym p2) (ibehead (lift ord0 j) s).
      by apply: (canLR (nscastK p1)); rewrite -nscast_trans nscast_id.
    rewrite !Hcast.
    congr (nscast _ (LCSLts _ _ _ _ _ (head _ _ :: _)) :: _).
    + rewrite -nscast_trans nscast_id; apply: congr1.
      rewrite -!map_comp; apply: eq_map => ? /=.
      by rewrite Hcast ibehead_behead nscast_behead.
    + rewrite -!map_comp; apply: eq_map => ? /=.
      by rewrite ibehead_head (nscast_head _ (esym p1)) Hcast.
    + rewrite -ibehead_lift0 IHt /= !Hcast !ibehead_behead !nscast_behead.
      apply: (canRL (nscastKV p2)); rewrite -nscast_trans nscast_id.
      apply: congr1; rewrite -!map_comp; apply: eq_map => ? /=.
      by rewrite Hcast ibehead_behead nscast_behead.
  - rewrite !ibehead0 !nscast_id /=; apply: congr1; apply: eq_map => ? /=.
    by rewrite ibehead0 nscast_id.
Qed.

Definition abehead {X n} (s : iter n seq X) :=
  foldr ibehead s (enum 'I_n).

Lemma abehead0 X (s : iter 0 seq X) : abehead s = s.
Proof.
  rewrite /abehead.
  by have ->: enum 'I_0 = [::] by rewrite enumT unlock.
Qed.

Lemma abehead_nil X n : abehead ([::] : iter n.+1 seq X) = [::].
Proof.
  rewrite /abehead.
  by elim: (enum 'I_n.+1) => [| j js IHjs] //=; rewrite IHjs ibehead_nil.
Qed.

Lemma abehead_NIL n : abehead (NIL : iter n seq (seq T)) = NIL.
Proof.
  rewrite /abehead.
  by elim: (enum 'I_n) => [| j js IHjs] //=; rewrite IHjs ibehead_NIL.
Qed.

Lemma abehead_cons X n x s : @abehead X n.+1 (x :: s) = map abehead s.
Proof.
  rewrite /abehead enum_ordS /= ibehead0.
  elim: (enum 'I_n) => [| j js IHjs] /=; first by rewrite map_id.
  by rewrite ibehead_lift0 behead_map IHjs -map_comp.
Qed.

Lemma abehead_head_behead X n d (s : iter n.+1 seq X) :
  abehead (head d (behead s)) = head (abehead d) (abehead s).
Proof.
  case: s => [| x s] /=; first by rewrite abehead_nil.
  by rewrite abehead_cons; case: s.
Qed.

Lemma abehead_behead X n (s : iter n.+1 seq X) :
  @abehead X n.+1 (behead s) = behead (abehead s).
Proof.
  case: n => [| n] in s *.
  - rewrite /abehead enum_ordS.
    have ->: enum 'I_0 = [::] by rewrite enumT unlock.
    by rewrite /= ibehead_behead.
  - case: s => [| x s] /=; first by rewrite abehead_nil.
    rewrite abehead_cons {x}.
    case: s => [| x s] /=; first by rewrite abehead_nil.
    by rewrite abehead_cons.
Qed.

Lemma nscast_abehead X m n (eq_mn : m = n) (s : iter m seq X) :
  nscast eq_mn (@abehead X m s) = @abehead X n (nscast eq_mn s).
Proof.  by case: _ / eq_mn.  Qed.

Lemma abehead_LCSLts ts x ys diag xneighbor neighbors:
  abehead (LCSLts ts x ys diag xneighbor neighbors) =
  nscast
    (size_map behead ts)
    (LCSLts (map behead ts) x ys
            (nscast (esym (size_map behead ts)) (abehead diag))
            (nscast (esym (size_map behead ts)) (abehead xneighbor))
            (map (nscast (esym (size_map behead ts)) \o abehead) neighbors)).
Proof.
  elim: ts => [| t ts IHts] /= in ys diag xneighbor neighbors *.
  - rewrite !nscast_id !LCSLtsE !abehead0 -[neighbors in LHS]map_id.
    by congr LCSL0; apply: eq_map => ? /=; rewrite nscast_id abehead0.
  - elim: t => [| y t IHt] in diag xneighbor neighbors *; rewrite !LCSLtsE /=;
      first by rewrite nscast_nil abehead_NIL.
    set p1 := size_map behead (t :: ts) in IHt *.
    set p2 := size_map behead ((y :: t) :: ts) in IHt *.
    rewrite abehead_cons.
    case: t => [| y' t] in p1 p2 IHt diag neighbors *;
      first by rewrite !LCSLtsE nscast_nil.
    move/(_ (behead diag) (behead xneighbor) (map behead neighbors)) in IHt.
    rewrite LCSLtsE /= abehead_cons in IHt.
    rewrite !LCSLtsE /= IHt (nscast_cons (size_map behead ts)).
    have Hcast (s : iter (size ts).+1 seq (seq T)) :
      nscast (esym p1) (@abehead _ (size ts).+1 (behead s))
      = behead (nscast (esym p2) (abehead s)).
    { rewrite -nscast_behead; apply: (canLR (nscastK p1)).
      by rewrite -nscast_trans nscast_id abehead_behead. }
    rewrite !Hcast; congr cons.
    + rewrite IHts /=.
      have Hcast1 (s : iter (size ts).+1 seq (seq T)) :
        nscast (esym (size_map behead ts)) (abehead (head NIL (behead s))) =
        head NIL (nscast (esym p2) (abehead s)).
      { rewrite abehead_head_behead abehead_NIL.
        by rewrite -(nscast_head (esym (size_map behead ts))). }
      rewrite -nscast_behead -abehead_behead !Hcast1.
      congr (nscast _ (LCSLts _ _ _ _ _ (_ :: _))).
      * { case: t => [| y'' t] in p1 p2 IHt Hcast Hcast1 *;
            first by rewrite !LCSLtsE abehead_NIL nscast_NIL.
          move/(congr1 (head NIL)): IHt; rewrite !LCSLtsE /= => ->.
          rewrite -(nscast_head (size_map behead ts)) nscastK /=.
          rewrite !Hcast -!map_comp -nscast_behead abehead_behead.
          congr (LCSLts _ _ _ _ _ (head _ _ :: _)).
          - by apply: congr1; apply: eq_map => ? /=; rewrite Hcast.
          - by apply: eq_map => ? /=; rewrite Hcast. }
      * by rewrite -!map_comp; apply: eq_map => ? /=; rewrite Hcast1.
    + apply: (canLR (nscastKV p1)); rewrite -nscast_trans nscast_id -!map_comp.
      by apply: congr1; apply: eq_map => ? /=; rewrite Hcast.
Qed.

Lemma lcsl_cons_aux ts x ys diag xneighbor neighbors :
  [::] \notin ts ->
  nhead (LCSLts ts x ys diag xneighbor neighbors) =
  let hds := map ohead ts in
  if (all (pred1 x) ys) && (all (pred1 (Some x)) hds)
  then x :: nhead (nscast (esym (size_map behead ts)) (abehead diag))
  else argmaxl
         size (nhead xneighbor)
         (catrev [tuple nhead
                        (LCSLts (set_nth [::] ts i (behead (nth [::] ts i)))
                                x ys
                                (nscast (esym (lcsl_ts_size i))
                                        (ibehead i diag))
                                (nscast (esym (lcsl_ts_size i))
                                        (ibehead i xneighbor))
                                (map
                                   (nscast (esym (lcsl_ts_size i)) \o ibehead i)
                                   neighbors))
                 | i < size ts]
                 (map nhead neighbors)).
Proof.
  elim: ts => [|[| y t] ts IHts] // in ys diag xneighbor neighbors *.
  - move=> _; rewrite /= andbT !LCSLtsE /LCSL0 /= abehead0.
    have ->: enum 'I_0 = [::] by rewrite enumT unlock.
    by rewrite nscast_id map_id.
  - rewrite inE orFb => Hts; rewrite LCSLtsE nheadE {}IHts //=.
    have ->: all (pred1 x) (y :: ys) && all (pred1 (Some x)) (map ohead ts) =
    all (pred1 x) ys && all (pred1 (Some x)) (map ohead ((y :: t) :: ts))
      by rewrite /= [in Some _ == Some _]/eq_op /= andbCA andbA.
    case: ifP => _.
    + rewrite abehead_head_behead abehead_NIL.
      by rewrite nhead_head -(nscast_head (esym (size_map behead ts))).
    + rewrite nhead_head enum_ordS map_cons -!map_comp.
      congr (argmaxl _ _ (catrev _ (_ :: _))).
      * { apply: eq_map => i /=; rewrite LCSLtsE /=.
          set p1 := lcsl_ts_size i.
          set p2 := @lcsl_ts_size ((y :: t) :: ts) (lift ord0 i).
          have Hcast_head (s : iter (size ts).+1 seq (seq T)) :
            nscast (esym p1) (ibehead i (head NIL s)) =
            head NIL (nscast (esym p2) (ibehead (lift ord0 i) s))
            by rewrite -(nscast_head (esym (lcsl_ts_size i))) ibehead_head.
          rewrite !Hcast_head -nscast_behead ibehead_behead.
          congr (nhead (LCSLts _ _ _ _ _ (_ :: _))).
          - rewrite (@ibehead_LCSLts (t :: ts) _ _ _ _ _ (lift ord0 i)).
            set p3 := @lcsl_ts_size (t :: ts) (lift ord0 i).
            have Hcast_behead (s : iter (size ts).+1 seq (seq T)) :
              nscast (esym p3) (ibehead (lift ord0 i) (behead s)) =
              behead (nscast (esym p2) (ibehead (lift ord0 i) s)).
            { rewrite -nscast_behead ibehead_behead.
              by apply: (canLR (nscastK p3)); rewrite -nscast_trans nscast_id. }
            rewrite -nscast_trans nscast_id nscast_behead !Hcast_behead.
            congr (head _ (LCSLts (_ :: _) _ _ _ _ _)).
            by rewrite -!map_comp; apply: eq_map => ? /=; rewrite Hcast_behead.
          - by rewrite -!map_comp; apply: eq_map => ? /=; rewrite Hcast_head. }
      * rewrite -nhead_head !nscast_id !ibehead0.
        congr (nhead (LCSLts (_ :: _) _ _ _ _ _)).
        by apply: eq_map => ? /=; rewrite nscast_id ibehead0.
      * by apply: eq_map => ? /=; rewrite nhead_head.
Qed.

Lemma lcsl_cons x s ts :
  [::] \notin ts ->
  LCSL (x :: s) ts =
  if all (pred1 (Some x)) (map ohead ts) then x :: LCSL s (map behead ts)
  else argmaxl size (LCSL s ts)
               (rev [tuple LCSL (x :: s) (set_nth [::] ts i
                                                  (behead (nth [::] ts i)))
                    | i < size ts]).
Proof.
  move=> Hts; rewrite /LCSL /= lcsl_cons_aux //=.
  congr (if _ then _ :: nhead _ else argmaxl _ _ (rev _)).
  - elim: s => [| x' s IHs]; first by rewrite abehead_NIL nscast_NIL.
    by rewrite /= abehead_LCSLts nscastK IHs.
  - apply: eq_map => i /=.
    suff ->: nscast (esym (lcsl_ts_size i)) (ibehead i (LCSLs s ts)) =
    LCSLs s (set_nth [::] ts i (behead (nth [::] ts i))) by [].
    elim: s => [| x' s IHs]; first by rewrite ibehead_NIL nscast_NIL.
    by rewrite /= ibehead_LCSLts nscastK IHs.
Qed.

Definition LCSL_spec s ts (lcsl : seq T) :=
  [/\ subseq lcsl s, all (subseq lcsl) ts &
      forall u, subseq u s -> all (subseq u) ts -> size u <= size lcsl].

Lemma lcsl_correct_nil_l ts : LCSL_spec [::] ts (LCSL [::] ts).
Proof.
  rewrite lcsl_nil_l; split; first by rewrite sub0seq.
  - by apply/allP => ?; rewrite sub0seq.
  - by move=> ?; rewrite subseq0 => /eqP ->.
Qed.

Lemma lcsl_correct_nil_r s ts : [::] \in ts -> LCSL_spec s ts (LCSL s ts).
Proof.
  move=> Hts; rewrite lcsl_nil_r //; split; first by rewrite sub0seq.
  - by apply/allP => ?; rewrite sub0seq.
  - by move=> ? _ /allP /(_ _ Hts); rewrite subseq0 => /eqP ->.
Qed.

Lemma lcsl_correct s ts : LCSL_spec s ts (LCSL s ts).
Proof.
  pose mes (s : seq T) (ts : seq (seq T)) := size s + (\sum_(t <- ts) size t).
  elim: {s ts}(mes s ts) {-2}s {-2}ts (leqnn (mes s ts)) => [| n IHn] s ts.
  - rewrite leqn0 addn_eq0 size_eq0 => /andP [/eqP -> _].
    exact: lcsl_correct_nil_l.
  - case: s => [| x s] Hmes; first by apply: lcsl_correct_nil_l.
    case: (boolP ([::] \in ts)) => [| Hts]; first by apply: lcsl_correct_nil_r.
    rewrite lcsl_cons //. case: ifP => Hx.
    + have /IHn [IH1 IH2 IH3] : mes s (map behead ts) <= n.
      { move: Hmes; rewrite /mes /= addSn ltnS; apply: leq_trans.
        rewrite leq_add2l big_map leq_sum // => t.
        by rewrite size_behead leq_pred. }
      split; first by rewrite /= eqxx.
      * apply/allP => t Ht; move/allP/(_ (ohead t)): Hx.
        rewrite map_f //= => /(_ isT) H; case: t H => // _ t /eqP [] -> in Ht *.
        by rewrite /= eqxx (allP IH2) // (map_f behead Ht).
      * { case=> // z u; rewrite [subseq _ _] /=.
          case: ifP => [/eqP |] Hzx Hus Hzuts.
          - apply: IH3 => //; apply/allP => _ /mapP [t Ht ->].
            move/allP/(_ (ohead t)): Hx.
            rewrite map_f //= => /(_ isT) H; case: t H Ht => // _ t /eqP [] ->.
            by move/(allP Hzuts); rewrite Hzx /= eqxx.
          - apply: leqW; apply: IH3 => //; apply/allP => _ /mapP [t Ht ->].
            move/allP/(_ (ohead t)): Hx.
            rewrite map_f //= => /(_ isT) H; case: t H Ht => // y t /eqP [] ->.
            by move/(allP Hzuts); rewrite /= Hzx. }
    + have /IHn [IHs1 IHs2 IHs3] : mes s ts <= n
        by move: Hmes; rewrite /mes /= addSn ltnS.
      have Htsi (i : 'I_(size ts)) :
        mes (x :: s) (set_nth [::] ts i (behead (nth [::] ts i))) <= n.
      { move: Hmes; rewrite /mes /= addSn ltnS; apply: leq_trans.
        rewrite addSn -addnS leq_add2l; set ts' := set_nth _ _ _ _.
        have Hsize: size ts' = size ts by rewrite size_set_nth; apply/maxn_idPr.
        have Hts' i0 : i0 != i :> nat -> nth [::] ts' i0 = nth [::] ts i0
          by move=> Hi0; rewrite nth_set_nth /= (negPf Hi0).
        rewrite !(big_nth [::]) (@big_cat_nat _ _ _ i.+1) //= Hsize //.
        rewrite [X in _ < X](@big_cat_nat _ _ _ i.+1) //=.
        rewrite !big_nat_recr //= -addSn -addnS !big_nat !leq_add //.
        - apply: eq_leq; apply: eq_bigr => i0 Hi0; rewrite Hts' //.
          by apply/negP => /eqP Hi0i; rewrite Hi0i ltnn andbF in Hi0.
        - rewrite nth_set_nth /= eqxx size_behead prednK //.
          case E: nth => //; case/negP: Hts; rewrite -E; apply/nthP.
          by exists i.
        - apply: eq_leq; apply: eq_bigr => i0 Hi0; rewrite Hts' //.
          by apply/negP => /eqP Hi0i; rewrite Hi0i ltnn in Hi0. }
      set lts' := rev _; split.
      * { case/predU1P: (argmaxl_mem size (LCSL s ts) lts') => [-> |].
          - by rewrite (subseq_trans IHs1) ?subseq_cons.
          - rewrite /lts' [rev _]/= -!map_rev.
            by case/mapP => i Hi ->; case/IHn: (Htsi i). }
      * case/predU1P: (argmaxl_mem size (LCSL s ts) lts') => [-> |] //.
        rewrite /lts' [rev _]/= -!map_rev.
        case/mapP => i Hi ->; set ts' := set_nth _ _ _ _; set lt' := LCSL _ _.
        have Hts' : size ts' = size ts
          by rewrite size_set_nth; apply: maxn_idPr.
        apply/allP => _ /(nthP [::]) [j Hj <-].
        case/IHn: (Htsi i) => _ /allP /(_ (nth [::] ts' j)).
        rewrite mem_nth ?Hts' // nth_set_nth => /(_ isT) H _.
        rewrite /= in H; case: ifP H => // /eqP ->; rewrite /lt' /ts'.
        by case: nth => // y t /subseq_trans -> //; rewrite subseq_cons.
      * { case=> // z u Hzuxs Hzuts; apply: argmaxl_sup; rewrite /= in Hzuxs.
          move: Hzuxs; case: ifP => [/eqP {z}-> | Hzx] in Hzuts *.
          - move=> Hus'; case/allPn: Hx => _ /mapP [t Ht ->].
            case: t Ht => [/(negP Hts) | y t Hyt] // => Hyx.
            rewrite /= /eq_op /= in Hyx; case/(nthP [::]): (Hyt) => i Hi Hnthi.
            apply/orP; right; apply/hasP.
            exists (nth [::] lts' (rev_ord (Ordinal Hi))).
            + by rewrite mem_nth // size_rev size_map size_enum_ord.
            + rewrite /lts' (nth_rev [::]) size_tuple.
              rewrite -[size _ - _]/(rev_ord _ : nat) rev_ordK nth_mktuple.
              case/IHn: (Htsi (Ordinal Hi)) => _ _ -> //.
              * by rewrite /= eqxx.
              * { apply/allP => t' /(nthP [::]) [j].
                  rewrite size_set_nth (maxn_idPr Hi) nth_set_nth /= => Hj <-.
                  case: ifP => [_ | Hij].
                  - move/allP/(_ (nth [::] ts i)): Hzuts.
                    by rewrite Hnthi /= eq_sym (negPf Hyx) => ->.
                  - move/allP/(_ (nth [::] ts j)): Hzuts => -> //.
                    by rewrite mem_nth. }
              * by rewrite ltn_ord.
          - by move/IHs3 => ->. }
Qed.

End LongestCommonSubsequenceForList.

Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [:: 2; 1; 3; 5] ].
     (* = [:: [:: [:: 2; 3; 5]; [:: 1; 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*       [:: [:: 2; 3; 5]; [:: 3; 5];    [:: 3; 5]; [:: 5]]; *)
     (*       [:: [:: 3; 5];    [:: 3; 5];    [:: 3; 5]; [:: 5]]; *)
     (*       [:: [:: 5];       [:: 5];       [:: 5];    [:: 5]]; *)
     (*       [:: [:: 5];       [:: 5];       [:: 5];    [:: 5]]] *)
     (* : seq (iter (size [:: [:: 2; 1; 3; 5]]) seq (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 3; 4; 5] [:: [:: 2; 1; 3; 5] ].
     (* = [:: [:: [:: 1; 3; 5]; [:: 1; 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*       [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*       [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*       [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]] *)
     (* : seq (iter (size [:: [:: 2; 1; 3; 5]]) seq (seq nat_eqType)) *)
Eval compute in LCSLs_debug [::] [:: [:: 2; 1; 3; 5] ].
     (* = [::] *)
     (* : seq (iter (size [:: [:: 2; 1; 3; 5]]) seq (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5] ].
     (* = [:: [:: [:: [:: 1; 3; 5]; [:: 1; 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]]; *)
     (*       [:: [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]]; *)
     (*       [:: [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 3; 5]; [:: 3; 5]; [:: 3; 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]]; *)
     (*       [:: [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]]; *)
     (*       [:: [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]; *)
     (*           [:: [:: 5]; [:: 5]; [:: 5]; [:: 5]]]] *)
     (* : seq *)
     (*     (iter (size [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]]) seq *)
     (*        (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [:: 1; 3; 4]; [:: 2; 1; 3; 5] ].
     (* = [:: [:: [:: [:: 1; 3]; [:: 1; 3]; [:: 3]; [::]]; *)
     (*           [:: [:: 3]; [:: 3]; [:: 3]; [::]]; [:: *)
     (*           [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [:: 3]; [:: 3]; [:: 3]; [::]]; *)
     (*           [:: [:: 3]; [:: 3]; [:: 3]; [::]]; [:: *)
     (*           [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [:: 3]; [:: 3]; [:: 3]; [::]]; *)
     (*           [:: [:: 3]; [:: 3]; [:: 3]; [::]]; [:: *)
     (*           [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]]] *)
     (* : seq *)
     (*     (iter (size [:: [:: 1; 3; 4]; [:: 2; 1; 3; 5]]) seq (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]; [:: 2; 1; 3]].
     (* = [:: [:: [:: [:: [:: 1; 3]; [:: 1; 3]; [:: 3]]; *)
     (*               [:: [:: 1; 3]; [:: 1; 3]; [:: 3]]; *)
     (*               [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [::]; [::]; [::]]]; *)
     (*           [:: [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [:: 3]; [:: 3]; [:: 3]]; [:: [:: 3]; [:: 3]; [:: 3]]; *)
     (*               [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]]; *)
     (*       [:: [:: [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [:: 3]; [:: 3]; [:: 3]]; [:: [:: 3]; [:: 3]; [:: 3]]; *)
     (*               [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [:: 3]; [:: 3]; [:: 3]]; [:: [:: 3]; [:: 3]; [:: 3]]; *)
     (*               [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]]; *)
     (*       [:: [:: [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [:: 3]; [:: 3]; [:: 3]]; [:: [:: 3]; [:: 3]; [:: 3]]; *)
     (*               [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [:: 3]; [:: 3]; [:: 3]]; [:: *)
     (*               [:: 3]; [:: 3]; [:: 3]]; [:: [:: 3]; [:: 3]; [:: 3]]; *)
     (*               [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]]; *)
     (*       [:: [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]]; *)
     (*       [:: [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]; *)
     (*           [:: [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]; *)
     (*               [:: [::]; [::]; [::]]; [:: [::]; [::]; [::]]]]] *)
     (* : seq *)
     (*     (iter (size [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]; [:: 2; 1; 3]]) seq *)
     (*        (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]; [::]].
     (* = [:: [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]]; *)
     (*       [:: [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]; *)
     (*           [:: [::]; [::]; [::]; [::]]; [:: [::]; [::]; [::]; [::]]]] *)
     (* : seq *)
     (*     (iter (size [:: [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]; [::]]) seq *)
     (*        (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [:: [::]; [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]].
     (* = [:: [::]; [::]; [::]; [::]; [::]] *)
     (* : seq *)
     (*     (iter (size [:: [::]; [:: 1; 3; 4; 5]; [:: 2; 1; 3; 5]]) seq *)
     (*        (seq nat_eqType)) *)
Eval compute in LCSLs_debug [:: 1; 2; 3; 4; 5] [::].
     (* = [:: [:: 1; 2; 3; 4; 5]; [:: 2; 3; 4; 5]; [:: *)
     (*       3; 4; 5]; [:: 4; 5]; [:: 5]] *)
     (* : seq (iter (size [::]) seq (seq nat_eqType)) *)
